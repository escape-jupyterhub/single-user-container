FROM rucio/jupyterlab:latest

RUN conda install -c conda-forge root
RUN pip install rucio-clients~=1.25.7

USER root

# ESCAPE grid-security and VOMS setup
RUN apt update && apt -y install software-properties-common \
    && wget -q -O - https://dist.eugridpma.info/distribution/igtf/current/GPG-KEY-EUGridPMA-RPM-3 | apt-key add -

RUN apt update \
    && add-apt-repository 'deb http://repository.egi.eu/sw/production/cas/1/current egi-igtf core' \
    && apt -y install ca-certificates ca-policy-egi-core

RUN mkdir -p /etc/vomses \
    && wget https://indigo-iam.github.io/escape-docs/voms-config/voms-escape.cloud.cnaf.infn.it.vomses -O /etc/vomses/voms-escape.cloud.cnaf.infn.it.vomses

RUN mkdir -p /etc/grid-security/vomsdir/escape \
    && wget https://indigo-iam.github.io/escape-docs/voms-config/voms-escape.cloud.cnaf.infn.it.lsc -O /etc/grid-security/vomsdir/escape/voms-escape.cloud.cnaf.infn.it.lsc

# Setup merged CERN CA file
RUN mkdir /certs \
    && touch /certs/rucio_ca.pem \
    && cat /etc/grid-security/certificates/CERN-Root-2.pem >> /certs/rucio_ca.pem \
    && cat /etc/grid-security/certificates/CERN-GridCA.pem >> /certs/rucio_ca.pem

# Setup extension Rucio instance config
ADD bin/configure.py /rucio-jl-docker/configure.py
ADD bin/configure.sh /rucio-jl-docker/configure.sh
RUN fix-permissions /rucio-jl-docker \
    && sed -i -e 's/\r$/\n/' /rucio-jl-docker/configure.sh

ENV RUCIO_MODE=download
ENV RUCIO_WILDCARD_ENABLED=1
ENV RUCIO_BASE_URL=https://escape-rucio.cern.ch
ENV RUCIO_AUTH_URL=https://escape-rucio-auth.cern.ch
ENV RUCIO_DISPLAY_NAME="ESCAPE Data Lake"
ENV RUCIO_NAME=ESCAPE
ENV RUCIO_SITE_NAME=cern
ENV RUCIO_VOMS_ENABLED=1
ENV RUCIO_VOMS_VOMSES_PATH=/etc/vomses
ENV RUCIO_VOMS_CERTDIR_PATH=/etc/grid-security/certificates
ENV RUCIO_VOMS_VOMSDIR_PATH=/etc/grid-security/vomsdir
ENV RUCIO_CA_CERT=/certs/rucio_ca.pem

# Auto load kernel extension when loading IPython
ENV IPYTHONDIR=/etc/ipython
ADD ipython_kernel_config.json /etc/ipython/profile_default/ipython_kernel_config.json
RUN chown -R $NB_UID /etc/ipython

USER $NB_UID

CMD ["/rucio-jl-docker/configure.sh", "start-notebook.sh"]
