#!/bin/bash
set -e
python /rucio-jl-docker/configure.py
exec "$@"